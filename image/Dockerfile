FROM registry.gitlab.com/clarin-eric/docker-alpine-fpm-base:1.8.5

LABEL maintainer="andre@clarin.eu"

ARG MATOMO_VERSION=5.1.0
ARG GPG_KEY=F529A27008477483777FC23D63BB30D0E5D2C749
ARG BOTTRACKER_VERSION=5.2.15
ARG DBIP_DATE_VERSION=2024-07
ARG MM_DBREADER_PHP_VERSION=1.11.1

ARG ZIP_VERSION=3.0-r12
ARG MARIADB_VERSION=10.11.6-r0
ARG JPEG_VERSION=9e-r1
ARG FREETYPE_DEV_VERSION=2.13.2-r0
ARG LIBPNG_VERSION=1.6.40-r0
ARG LIBMAXMINDDB_VERSION=1.7.1-r2
ARG GNUPG_VERSION=2.4.4-r0
ARG LIBTOOL_VERSION=2.4.7-r3
ARG BUILD_BASE_VERSION=0.5-r3
ARG XZ_VERSION=5.4.5-r0
ARG RE2C_VERSION=3.1-r0

WORKDIR /tmp
RUN apk update \
    && apk add --no-cache \
    mariadb-client="${MARIADB_VERSION}" \
    jpeg-dev="${JPEG_VERSION}" \
    freetype-dev="${FREETYPE_DEV_VERSION}" \
    libpng-dev="${LIBPNG_VERSION}" \
    libmaxminddb-dev="${LIBMAXMINDDB_VERSION}" \ 
    libtool="${LIBTOOL_VERSION}" \ 
    zip="${ZIP_VERSION}" \
    xz="${XZ_VERSION}" \
    && apk add --no-cache --virtual .build-deps \
    php83-pear="${PHP8_VERSION}" \
    gnupg="${GNUPG_VERSION}" \
    build-base="${BUILD_BASE_VERSION}" \
    re2c="${RE2C_VERSION}" \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && curl -fsSL -o matomo.tar.gz "https://builds.matomo.org/matomo-${MATOMO_VERSION}.tar.gz" \
    && curl -fsSL -o matomo.tar.gz.asc "https://builds.matomo.org/matomo-${MATOMO_VERSION}.tar.gz.asc" \
    && ( gpg --keyserver keyserver.ubuntu.com --recv-keys ${GPG_KEY} \
    || gpg --keyserver keys.gnupg.net --recv-keys ${GPG_KEY} \
    || gpg --keyserver pgp.mit.edu --recv-keys ${GPG_KEY} \
    || gpg --keyserver ha.pool.sks-keyservers.net --recv-keys ${GPG_KEY} \
    || gpg --keyserver keyserver.pgp.com --recv-keys ${GPG_KEY} ) \
    && gpg --batch --verify matomo.tar.gz.asc matomo.tar.gz \
    && gpgconf --kill all \
    && rm -rf "$GNUPGHOME" matomo.tar.gz.asc \
    && tar -xzf matomo.tar.gz -C /var/www/ \
    && curl -fsSL -o dbip-city-lite-${DBIP_DATE_VERSION}.mmdb.gz https://download.db-ip.com/free/dbip-city-lite-${DBIP_DATE_VERSION}.mmdb.gz \
    && gunzip dbip-city-lite-${DBIP_DATE_VERSION}.mmdb.gz \
    && mv dbip-city-lite-${DBIP_DATE_VERSION}.mmdb /var/www/matomo/misc/DBIP-City.mmdb \
    && curl -fsSL -o MaxMind-DB-Reader-php.tar.gz https://github.com/maxmind/MaxMind-DB-Reader-php/archive/v${MM_DBREADER_PHP_VERSION}.tar.gz \
    && tar xvf MaxMind-DB-Reader-php.tar.gz \
    && bash -c "cd MaxMind-DB-Reader-php-*/ext \
    && ln -s /usr/bin/php-config83 /usr/bin/php-config \
    && phpize83 \
    && ./configure \
    && make \
    && make install" \
    && curl -fsSL -o BotTracker.zip https://plugins.matomo.org//api/2.0/plugins/BotTracker/download/${BOTTRACKER_VERSION} \
    && unzip BotTracker.zip \
    && mv BotTracker /var/www/matomo/plugins \
    && touch /var/log/matomo-archive.log \
    && touch /var/log/matomo-app.log \
    && chown nginx:nginx /var/log/matomo-archive.log /var/log/matomo-app.log  \
    && apk del .build-deps \
    && rm -rf /tmp/*

# Copy Matomo customizations 
COPY matomo/matomo-branding /var/www/matomo/misc/user
COPY matomo/clarintheme /var/www/matomo/plugins/ClarinTheme
COPY matomo/matomo-config/config.ini.php.tpl /matomo-config.d/

# Copy PHP geoip configuration
COPY php/maxminddb.ini /etc/php83/conf.d/maxminddb.ini
COPY php/php.ini /etc/php83/conf.d/
COPY php/opcache-recommended.ini /etc/php83/conf.d/
COPY php/www.conf /etc/php83/php-fpm.d/

# Matomo archive cron
COPY cron/fpm-data-crontab /etc/crontabs/$FPM_USER

COPY init.sh /init/docker-alpine-fpm-matomo-init.sh

WORKDIR /var/www/matomo