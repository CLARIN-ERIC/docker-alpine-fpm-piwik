#!/bin/bash

initialize() {

    if [ ! -f "config/config.ini.php" ]; then
        if [ -d /matomo-config.d ]; then
            echo 'Initializing Matomo with supplied configuration'
            eval "cat <<EOF $(</matomo-config.d/config.ini.php.tpl)" 2> /dev/null 1> config/config.ini.php
        else
            exit
        fi
    fi

    sed -i "/\[General\]/a maintenance_mode = 1" config/config.ini.php
    REC_STAT=$(grep "^record_statistics = " config/config.ini.php)
    sed -i "/^record_statistics = 0/d" config/config.ini.php
    sed -i "/\[Tracker\]/a record_statistics = 0" config/config.ini.php

    MYSQL_USER=$(grep -m 1 "^username =" "config/config.ini.php" |cut -d "=" -f 2 |cut -d "\"" -f 2|cut -d " " -f 2)
    MYSQL_PASSWORD=$(grep -m 1 "^password =" "config/config.ini.php" |cut -d "=" -f 2 |cut -d "\"" -f 2|cut -d " " -f 2)
    MYSQL_HOST=$(grep -m 1 "^host =" "config/config.ini.php" |cut -d "=" -f 2 |cut -d "\"" -f 2|cut -d " " -f 2)

    # Wait for mysql db to be available
    until mysql -u "${MYSQL_USER}" -p"${MYSQL_PASSWORD}" -h "${MYSQL_HOST}"  -e 'exit' &> /dev/null; do
        echo "Mysql database is unavailable - sleeping"
        sleep 20
    done

    echo 'Updating Matomo...'
    php console core:update --yes
    php console plugin:activate ClarinTheme

    sed -i "/maintenance_mode = 1/d" config/config.ini.php
    sed -i "/^record_statistics = 0/d" config/config.ini.php
    if [ -n "${REC_STAT}" ]; then
        sed -i "/\[Tracker\]/a ${REC_STAT}" config/config.ini.php
    fi

    php console core:archive &

    unset MYSQL_USER
    unset MYSQL_PASSWORD
    unset MYSQL_HOST
}

DECL=$(declare -f initialize)

chown -R "${FPM_USER}":"${FPM_USER}" .

eval "cat <<EOF $(</etc/crontabs/nginx)" 2> /dev/null 1> /tmp/nginx_tmp
mv /tmp/nginx_tmp /etc/crontabs/nginx

su -s "/bin/bash" -c "$DECL;initialize" "${FPM_USER}"
