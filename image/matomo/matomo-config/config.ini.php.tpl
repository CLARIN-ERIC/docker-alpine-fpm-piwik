
; <?php exit; ?> DO NOT REMOVE THIS LINE
; file automatically generated or modified by Matomo; you can manually override the default values in global.ini.php by redefining them in this file.
[database]
host = "${MATOMODB_HOST}"
username = "${MATOMODB_USER}"
password = "${MATOMODB_PASSWORD}"
dbname = "${MATOMODB}"
tables_prefix = "piwik_"
charset = "utf8mb4"
adapter = PDO\MYSQL                                                              
type = InnoDB                                                                    
schema = Mariadb

[General]
multi_server_environment = ${MATOMO_MULTISERVER}
proxy_client_headers[] = "HTTP_X_FORWARDED_FOR"
proxy_host_headers[] = "HTTP_X_FORWARDED_HOST"
salt = "c4732914cd83d86efe91f9e45fc8aa02"
noreply_email_address = "${MATOMO_NOREPLY_ADDR}"
login_password_recovery_email_name = CLARIN Matomo
login_password_recovery_email_address = "password-recovery.stats@clarin.eu"
login_password_recovery_replyto_email_address = "sysops@clarin.eu"
login_password_recovery_replyto_email_name = CLARIN sysops
force_ssl = 1
browser_archiving_disabled_enforce = 1
process_new_segments_from = "beginning_of_time"
$(
MATOMO_TRUSTED_HOSTS=${MATOMO_TRUSTED_HOSTS//[\(,\)]/}
HOSTS=(${MATOMO_TRUSTED_HOSTS})
for i in "${HOSTS[@]}"
do
  echo trusted_hosts[] = $i
done
)

; Disable OPTIMIZE TABLE statement which can create dead-locks
; and prevent MySQL backups
enable_sql_optimize_queries = 1

[log]
log_writers[] = file
log_writers[] = "screen"
log_level = ${MATOMO_LOG_LEVEL}
logger_file_path = /var/log/matomo-app.log

[CustomReports]
custom_reports_max_execution_time = 2700
custom_reports_disabled_dimensions = "CoreHome.VisitLastActionDate"

[Tracker]
window_look_back_for_visitor = 2592000
record_statistics = $(if [ "$MATOMO_READ_ONLY" = 1 ]; then echo "0"; else echo "1"; fi)

[mail]
transport = "smtp"
port = 25
host = "mail"

[Plugins]
Plugins[] = "CorePluginsAdmin"
Plugins[] = "CoreAdminHome"
Plugins[] = "CoreHome"
Plugins[] = "WebsiteMeasurable"
Plugins[] = "Diagnostics"
Plugins[] = "CoreVisualizations"
Plugins[] = "Proxy"
Plugins[] = "API"
Plugins[] = "Widgetize"
Plugins[] = "Transitions"
Plugins[] = "LanguagesManager"
Plugins[] = "Actions"
Plugins[] = "Dashboard"
Plugins[] = "MultiSites"
Plugins[] = "Referrers"
Plugins[] = "UserLanguage"
Plugins[] = "DevicesDetection"
Plugins[] = "Goals"
Plugins[] = "Ecommerce"
Plugins[] = "SEO"
Plugins[] = "Events"
Plugins[] = "UserCountry"
Plugins[] = "GeoIp2"
Plugins[] = "VisitsSummary"
Plugins[] = "VisitFrequency"
Plugins[] = "VisitTime"
Plugins[] = "VisitorInterest"
Plugins[] = "Feedback"
Plugins[] = "Monolog"
Plugins[] = "Login"
Plugins[] = "UsersManager"
Plugins[] = "SitesManager"
Plugins[] = "Installation"
Plugins[] = "CoreUpdater"
Plugins[] = "CoreConsole"
Plugins[] = "ScheduledReports"
Plugins[] = "UserCountryMap"
Plugins[] = "Live"
Plugins[] = "CustomVariables"
Plugins[] = "PrivacyManager"
Plugins[] = "ImageGraph"
Plugins[] = "Annotations"
Plugins[] = "MobileMessaging"
Plugins[] = "Overlay"
Plugins[] = "SegmentEditor"
Plugins[] = "Insights"
Plugins[] = "Morpheus"
Plugins[] = "Contents"
Plugins[] = "BulkTracking"
Plugins[] = "Resolution"
Plugins[] = "DevicePlugins"
Plugins[] = "Heartbeat"
Plugins[] = "Intl"
Plugins[] = "Marketplace"
Plugins[] = "ProfessionalServices"
Plugins[] = "UserId"
Plugins[] = "DBStats"
Plugins[] = "ExampleAPI"
Plugins[] = "MobileAppMeasurable"
Plugins[] = "Provider"
Plugins[] = "TagManager"
;Plugins[] = "BotTracker"
Plugins[] = "ClarinTheme"

[PluginsInstalled]
PluginsInstalled[] = "Login"
PluginsInstalled[] = "CoreAdminHome"
PluginsInstalled[] = "UsersManager"
PluginsInstalled[] = "SitesManager"
PluginsInstalled[] = "Installation"
PluginsInstalled[] = "CorePluginsAdmin"
PluginsInstalled[] = "CoreHome"
PluginsInstalled[] = "CoreVisualizations"
PluginsInstalled[] = "Proxy"
PluginsInstalled[] = "API"
PluginsInstalled[] = "Widgetize"
PluginsInstalled[] = "Transitions"
PluginsInstalled[] = "LanguagesManager"
PluginsInstalled[] = "Actions"
PluginsInstalled[] = "Dashboard"
PluginsInstalled[] = "MultiSites"
PluginsInstalled[] = "Referrers"
PluginsInstalled[] = "Goals"
PluginsInstalled[] = "SEO"
PluginsInstalled[] = "Events"
PluginsInstalled[] = "UserCountry"
PluginsInstalled[] = "VisitsSummary"
PluginsInstalled[] = "VisitFrequency"
PluginsInstalled[] = "VisitTime"
PluginsInstalled[] = "VisitorInterest"
PluginsInstalled[] = "ExampleAPI"
PluginsInstalled[] = "ExampleRssWidget"
PluginsInstalled[] = "Provider"
PluginsInstalled[] = "Feedback"
PluginsInstalled[] = "CoreUpdater"
PluginsInstalled[] = "CoreConsole"
PluginsInstalled[] = "ScheduledReports"
PluginsInstalled[] = "UserCountryMap"
PluginsInstalled[] = "Live"
PluginsInstalled[] = "CustomVariables"
PluginsInstalled[] = "PrivacyManager"
PluginsInstalled[] = "ImageGraph"
PluginsInstalled[] = "Annotations"
PluginsInstalled[] = "MobileMessaging"
PluginsInstalled[] = "Overlay"
PluginsInstalled[] = "SegmentEditor"
PluginsInstalled[] = "Insights"
PluginsInstalled[] = "Morpheus"
PluginsInstalled[] = "DevicesDetection"
PluginsInstalled[] = "Contents"
PluginsInstalled[] = "BulkTracking"
PluginsInstalled[] = "Resolution"
PluginsInstalled[] = "DevicePlugins"
PluginsInstalled[] = "UserLanguage"
PluginsInstalled[] = "Ecommerce"
PluginsInstalled[] = "Monolog"
PluginsInstalled[] = "Diagnostics"
PluginsInstalled[] = "WebsiteMeasurable"
PluginsInstalled[] = "Intl"
PluginsInstalled[] = "Heartbeat"
PluginsInstalled[] = "UserId"
PluginsInstalled[] = "ProfessionalServices"
PluginsInstalled[] = "ClarinTheme"
PluginsInstalled[] = "DBStats"
PluginsInstalled[] = "TagManager"
PluginsInstalled[] = "Marketplace"
PluginsInstalled[] = "GeoIp2"
PluginsInstalled[] = "MobileAppMeasurable"
;PluginsInstalled[] = "BotTracker"
PluginsInstalled[] = "IntranetMeasurable"
PluginsInstalled[] = "RssWidget"
PluginsInstalled[] = "Tour"
PluginsInstalled[] = "TwoFactorAuth"

